FROM node:12.13-alpine

# update packages
RUN apk update

WORKDIR /app

COPY package*.json ./
COPY tsconfig.json ./

COPY src /app/src

RUN npm install
RUN npm run build


COPY config.env .
CMD ["node", "./dist/src/deidentify-pdb/deidentify-main.js"]