<?php
    function get_access_token($url, $data){
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true, 
                CURLOPT_TIMEOUT => 0,  
                CURLOPT_POST => true,  
                CURLOPT_POSTFIELDS => $data 
            ));
        
        $response = curl_exec($curl);
        
        curl_close ($curl);
        
        $json_respone = json_decode($response);
        return $json_respone->{'access_token'};
    }

    function get_treatment_link($get_treatment_link_url,$access_token, $params){
        $curl = curl_init();
        $data = http_build_query($params);
        $url = $get_treatment_link_url."?".$data;
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTPHEADER => array('Authorization: Bearer '.$access_token)
            ));
        $response = curl_exec($curl);
        $errno = curl_errno($curl);
        $err = curl_error($curl);
        $info = curl_getinfo($curl);
        return $response;
        
    }

    $arr = [
        "username" => "xxxx",
        "password" => "xxx",
        "grant_type" => "password",
        "client_id" => "kidplus",
        "arr[0]" => 0,
        "arr[1]" => 1,
        "arr[2]" => 2,
        "arr[3]" => 3
    ];

    $data = http_build_query($arr);
    $auth_url = "https://keycloak.kidplus.fairtility.com/auth/realms/fairtility/protocol/openid-connect/token";

    $access_token = get_access_token($auth_url, $data);

    $get_treatment_link_url = "https://api.kidplus.fairtility.com/treatments/link";
    $treatment_link_params = [
        "name" => "1111",
        "patientGovId" => "2222",
    ];
    $treatment_url = get_treatment_link($get_treatment_link_url,$access_token,$treatment_link_params);
    echo $treatment_url;
?>
