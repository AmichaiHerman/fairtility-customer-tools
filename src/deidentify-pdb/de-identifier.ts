import * as dotenv from 'dotenv';
import * as path from 'path';
import {PatientsDbService} from "../services/db/patients-db.service";
dotenv.config({ path: path.join(process.cwd(), 'config.env') });

export class DeIdentifier{
    public async deidentifyDBs() {
        return await this.deidentifyPatientsDB();
    }

    private async deidentifyPatientsDB():Promise<string> {
        const patientsDB = await new PatientsDbService(process.env.PATIENS_DB_PATH);
        console.log(`patients DB path set to ${process.env.PATIENS_DB_PATH}`);
        await patientsDB.connect();
        if(Number(process.env.SET_PATIENT_ID_AS_NAME) || 0){
            await patientsDB.setPatientIdAsName();
        } else {
            await patientsDB.removePatientName();
        }

        if(Number(process.env.FILTER_OUT_HISTORICAL_PATIENTS)){
            console.log(`Removing from DB patients that do not have slides currently on the EmbryoScope`);
            await patientsDB.removeHistoricalPatients();
        }
        patientsDB.close();
        return '';
    }
}