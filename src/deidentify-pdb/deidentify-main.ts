
import {DeIdentifier} from "./de-identifier";
(async () => {
    console.log('Starting de identification process...')
    const deidentifier = new DeIdentifier();
    await deidentifier.deidentifyDBs();
})();