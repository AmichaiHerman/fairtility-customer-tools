import * as sqlite3 from "sqlite3";
import { Database } from 'sqlite/build/Database';
import { open } from 'sqlite';

export abstract class DbBaseService {
  protected _db: Database = null;

  protected constructor(protected dbFilePath: string) {
  }

  /************* connect *************/
  async connect() {
    console.log(`Connecting to ${this.dbFilePath}`);
    this._db = await open({
      filename: this.dbFilePath,
      driver: sqlite3.Database,
    });
  }

  /************* close *************/
  async close() {
    await this._db.close();
    console.log(`Closing ${this._db.config.filename}`);
  }

  get db() {
    return this._db;
  }
}
