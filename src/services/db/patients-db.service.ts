import { DbBaseService } from './db-base.service';
import {OngoingDbService} from "./ongoing-db.service";

export class PatientsDbService extends DbBaseService {
  constructor(dbFilePath: string) {
    super(dbFilePath);
  }

  public async removePatientName() {
    console.log(`Setting patient name as empty string`);
    const emptyString = "";
    return await this.db.run('UPDATE IDTable set Name=?',[emptyString]);
  }

  public async setPatientIdAsName(){
    console.log(`Setting patient ID as patient name`);
    return await this.db.run('update IDTable set Name=Id');
  }

  async getIdxBySlidesIds(slideIds: any[]): Promise<string[]> {
    const results: any[] = [];
    for (const id of slideIds) {
      if (id == '') {
        console.error('Found an empty slide id, skipping');
        continue;
      }

      console.debug(`Fetching patient Idx for slide id ${id.Id}`);
      const row = await this.getIdxBySinglelideId(id.Id);
      if(row != null){
        results.push(row);
      }
    }
    const uniqueResults = results.filter(
        (value, index) => results.indexOf(value) === index,
    );
    return uniqueResults;
  }

  async getIdxBySinglelideId(slideId: string): Promise<string> {
    const result = await this._db.get(
        'select Idx from SlideID where Slide = ?',
        [slideId],
    );
    if(result == null){
      return null;
    }
    return result.Idx;
  }

  private async filterOutPatientsByIdx(ongoingPatients: string[]) {
   let inQuery = "";

    inQuery = "(";
    let first = true;
    ongoingPatients.forEach((patient)=>{
      if (first) {
        first = false;
        inQuery += "'" + patient + "'";
      } else {
        inQuery+= ", '" + patient + "'";
      }
    })
    inQuery += ")";
    console.log(`Removing all patients that are not in the following list ${ongoingPatients.toString()}`);
    await this.db.run(`DELETE from Patients where Idx NOT IN ${inQuery}`);
    await this.db.run(`DELETE from IDTable where Idx NOT IN ${inQuery}`);
  }

  public async removeHistoricalPatients() {
    const ongoingDb = await new OngoingDbService(process.env.ONGOING_DB_PATH);
    await ongoingDb.connect();
    const ongoingSlides = await ongoingDb.getSlideIds();
    const ongoingPatients = await this.getIdxBySlidesIds(ongoingSlides);
    await this.filterOutPatientsByIdx(ongoingPatients);
  }
}
