import {DbBaseService} from "./db-base.service";

export class OngoingDbService extends DbBaseService {
    constructor(dbFilePath: string) {
        super(dbFilePath);
    }

    async getSlideIds() {
        return await this._db.all<any>(
            'SELECT Id FROM OnGoing GROUP BY Id',
            (err) => {
                console.error(err);
            },
        );
    }
}